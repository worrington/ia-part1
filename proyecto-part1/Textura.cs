﻿/*
 * Created by SharpDevelop.
 * User: Montserrat
 * Date: 18/03/2019
 * Time: 06:41 p. m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;

namespace proyecto_part1
{
	/// <summary>
	/// Description of Textura.
	/// </summary>
	public class Textura
	{	
		public string idTextura;
		public string nombreTextura;
		public Image imgTextura;
		
		public Textura(string _idTextura, string _nombreTextura, Image _textura)
		{
			this.idTextura = _idTextura;
			this.nombreTextura = _nombreTextura;
			this.imgTextura = _textura;
		}
	}
}
