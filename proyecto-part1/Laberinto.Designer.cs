﻿/*
 * Created by SharpDevelop.
 * User: CesarArleyOE
 * Date: 16/03/2019
 * Time: 04:16 p. m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace proyecto_part1
{
	partial class Laberinto
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem cargarLaberintoToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem personalesToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem crearPersonajesToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem algoritmosToolStripMenuItem;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
		private System.Windows.Forms.ToolStripMenuItem colocarPuntoDePartidaAquíToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem colocarMetaAquíToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem verPropiedadesToolStripMenuItem;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.ToolStripMenuItem proximamenteToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem buscarToolStripMenuItem;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.ToolStripTextBox tSBuscar;
		private System.Windows.Forms.ToolStripStatusLabel sLPosicion;
		private System.Windows.Forms.ToolStripMenuItem seleccionarPersonajesToolStripMenuItem;
		private System.Windows.Forms.ToolStripStatusLabel sLTextura;
		private System.Windows.Forms.ToolStripMenuItem sobreToolStripMenuItem;
		

		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.cargarLaberintoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.personalesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.crearPersonajesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.seleccionarPersonajesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.algoritmosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.proximamenteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.buscarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.tSBuscar = new System.Windows.Forms.ToolStripTextBox();
			this.sobreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.colocarPuntoDePartidaAquíToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.colocarMetaAquíToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.verPropiedadesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.sLPosicion = new System.Windows.Forms.ToolStripStatusLabel();
			this.sLTextura = new System.Windows.Forms.ToolStripStatusLabel();
			this.menuStrip1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.contextMenuStrip1.SuspendLayout();
			this.statusStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.archivoToolStripMenuItem,
			this.personalesToolStripMenuItem,
			this.algoritmosToolStripMenuItem,
			this.buscarToolStripMenuItem,
			this.sobreToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(600, 24);
			this.menuStrip1.TabIndex = 1;
			// 
			// archivoToolStripMenuItem
			// 
			this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.cargarLaberintoToolStripMenuItem,
			this.salirToolStripMenuItem});
			this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
			this.archivoToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
			this.archivoToolStripMenuItem.Text = "Archivo";
			// 
			// cargarLaberintoToolStripMenuItem
			// 
			this.cargarLaberintoToolStripMenuItem.Name = "cargarLaberintoToolStripMenuItem";
			this.cargarLaberintoToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
			this.cargarLaberintoToolStripMenuItem.Text = "Cargar Laberinto";
			this.cargarLaberintoToolStripMenuItem.Click += new System.EventHandler(this.CargarLaberintoToolStripMenuItemClick);
			// 
			// salirToolStripMenuItem
			// 
			this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
			this.salirToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
			this.salirToolStripMenuItem.Text = "Salir";
			this.salirToolStripMenuItem.Click += new System.EventHandler(this.SalirToolStripMenuItemClick);
			// 
			// personalesToolStripMenuItem
			// 
			this.personalesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.crearPersonajesToolStripMenuItem,
			this.seleccionarPersonajesToolStripMenuItem});
			this.personalesToolStripMenuItem.Name = "personalesToolStripMenuItem";
			this.personalesToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
			this.personalesToolStripMenuItem.Text = "Personajes";
			this.personalesToolStripMenuItem.Click += new System.EventHandler(this.PersonajesToolStripMenuItemClick);
			// 
			// crearPersonajesToolStripMenuItem
			// 
			this.crearPersonajesToolStripMenuItem.Name = "crearPersonajesToolStripMenuItem";
			this.crearPersonajesToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
			this.crearPersonajesToolStripMenuItem.Text = "Crear Personajes";
			this.crearPersonajesToolStripMenuItem.Click += new System.EventHandler(this.CrearPersonajesToolStripMenuItemClick);
			// 
			// seleccionarPersonajesToolStripMenuItem
			// 
			this.seleccionarPersonajesToolStripMenuItem.Name = "seleccionarPersonajesToolStripMenuItem";
			this.seleccionarPersonajesToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
			this.seleccionarPersonajesToolStripMenuItem.Text = "Seleccionar Personaje";
			this.seleccionarPersonajesToolStripMenuItem.Visible = false;
			// 
			// algoritmosToolStripMenuItem
			// 
			this.algoritmosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.proximamenteToolStripMenuItem});
			this.algoritmosToolStripMenuItem.Name = "algoritmosToolStripMenuItem";
			this.algoritmosToolStripMenuItem.Size = new System.Drawing.Size(78, 20);
			this.algoritmosToolStripMenuItem.Text = "Algoritmos";
			// 
			// proximamenteToolStripMenuItem
			// 
			this.proximamenteToolStripMenuItem.Enabled = false;
			this.proximamenteToolStripMenuItem.Name = "proximamenteToolStripMenuItem";
			this.proximamenteToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.proximamenteToolStripMenuItem.Text = "Proximamente";
			// 
			// buscarToolStripMenuItem
			// 
			this.buscarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.tSBuscar});
			this.buscarToolStripMenuItem.Name = "buscarToolStripMenuItem";
			this.buscarToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
			this.buscarToolStripMenuItem.Text = "Buscar";
			// 
			// tSBuscar
			// 
			this.tSBuscar.Name = "tSBuscar";
			this.tSBuscar.Size = new System.Drawing.Size(100, 23);
			this.tSBuscar.ToolTipText = "Escribe la columna y la fila que deseas buscar.\r\nsiguiendo este patron: C,F\r\nPor " +
	"ejemplo: E,4\r\nPresione enter para realizar la busqueda";
			this.tSBuscar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TSBuscarKeyDown);
			// 
			// sobreToolStripMenuItem
			// 
			this.sobreToolStripMenuItem.Name = "sobreToolStripMenuItem";
			this.sobreToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
			this.sobreToolStripMenuItem.Text = "Sobre...";
			this.sobreToolStripMenuItem.Click += new System.EventHandler(this.SobreToolStripMenuItemClick);
			// 
			// toolStripMenuItem1
			// 
			this.toolStripMenuItem1.Name = "toolStripMenuItem1";
			this.toolStripMenuItem1.Size = new System.Drawing.Size(125, 20);
			this.toolStripMenuItem1.Text = "toolStripMenuItem1";
			// 
			// pictureBox1
			// 
			this.pictureBox1.BackColor = System.Drawing.Color.DimGray;
			this.pictureBox1.Location = new System.Drawing.Point(0, 24);
			this.pictureBox1.Margin = new System.Windows.Forms.Padding(0);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(600, 600);
			this.pictureBox1.TabIndex = 0;
			this.pictureBox1.TabStop = false;
			this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.PictureBox1Paint);
			this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PictureBox1MouseClick);
			// 
			// contextMenuStrip1
			// 
			this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.colocarPuntoDePartidaAquíToolStripMenuItem,
			this.colocarMetaAquíToolStripMenuItem,
			this.verPropiedadesToolStripMenuItem});
			this.contextMenuStrip1.Name = "contextMenuStrip1";
			this.contextMenuStrip1.Size = new System.Drawing.Size(242, 70);
			// 
			// colocarPuntoDePartidaAquíToolStripMenuItem
			// 
			this.colocarPuntoDePartidaAquíToolStripMenuItem.Image = global::proyecto_part1.Resource1.bandera_inicio;
			this.colocarPuntoDePartidaAquíToolStripMenuItem.Name = "colocarPuntoDePartidaAquíToolStripMenuItem";
			this.colocarPuntoDePartidaAquíToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
			this.colocarPuntoDePartidaAquíToolStripMenuItem.Text = "Colocar punto de partida aquí...";
			this.colocarPuntoDePartidaAquíToolStripMenuItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
			this.colocarPuntoDePartidaAquíToolStripMenuItem.Click += new System.EventHandler(this.ColocarPuntoDePartidaAquíToolStripMenuItemClick);
			// 
			// colocarMetaAquíToolStripMenuItem
			// 
			this.colocarMetaAquíToolStripMenuItem.Image = global::proyecto_part1.Resource1.bandera_meta;
			this.colocarMetaAquíToolStripMenuItem.Name = "colocarMetaAquíToolStripMenuItem";
			this.colocarMetaAquíToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
			this.colocarMetaAquíToolStripMenuItem.Text = "Colocar meta aquí...";
			this.colocarMetaAquíToolStripMenuItem.Click += new System.EventHandler(this.ColocarMetaAquíToolStripMenuItemClick);
			// 
			// verPropiedadesToolStripMenuItem
			// 
			this.verPropiedadesToolStripMenuItem.Image = global::proyecto_part1.Resource1.propiedades;
			this.verPropiedadesToolStripMenuItem.Name = "verPropiedadesToolStripMenuItem";
			this.verPropiedadesToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
			this.verPropiedadesToolStripMenuItem.Text = "Ver Propiedades";
			this.verPropiedadesToolStripMenuItem.Click += new System.EventHandler(this.VerPropiedadesToolStripMenuItemClick);
			// 
			// statusStrip1
			// 
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.sLPosicion,
			this.sLTextura});
			this.statusStrip1.Location = new System.Drawing.Point(0, 624);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(600, 22);
			this.statusStrip1.TabIndex = 2;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// sLPosicion
			// 
			this.sLPosicion.Name = "sLPosicion";
			this.sLPosicion.Size = new System.Drawing.Size(0, 17);
			// 
			// sLTextura
			// 
			this.sLTextura.Name = "sLTextura";
			this.sLTextura.Size = new System.Drawing.Size(0, 17);
			// 
			// Laberinto
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(600, 646);
			this.Controls.Add(this.statusStrip1);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.menuStrip1);
			this.MainMenuStrip = this.menuStrip1;
			this.MaximizeBox = false;
			this.Name = "Laberinto";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Laberinto";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LaberintoFormClosing);
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LaberintoKeyDown);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.contextMenuStrip1.ResumeLayout(false);
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		
	}
}

