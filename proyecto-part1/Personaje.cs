﻿/*
 * Created by SharpDevelop.
 * User: Montserrat
 * Date: 19/03/2019
 * Time: 01:23 a. m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;

namespace proyecto_part1
{
	/// <summary>
	/// Description of Personaje.
	/// </summary>
	public class Personaje
	{
		public string nombrePersonaje;
		public Image img;
		public List<PesoTextura> pesos;
		
		public Personaje(String _nombrePersonaje, Image _img, List<PesoTextura> _pesos)
		{
			nombrePersonaje = _nombrePersonaje;
			img = _img;
			pesos = _pesos;			
		}
	}
}
