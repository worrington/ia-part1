﻿/*
 * Created by SharpDevelop.
 * User: Montserrat
 * Date: 19/03/2019
 * Time: 02:24 a. m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace proyecto_part1
{
	/// <summary>
	/// Description of PesoTextura.
	/// </summary>
	public class PesoTextura
	{
		public float peso;
		public string idTextura;
		
		public PesoTextura(string _idTextura, float _peso)
		{
			peso = _peso;
			idTextura = _idTextura;
		}
	}
}
