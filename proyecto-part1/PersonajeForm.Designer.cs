﻿/*
 * Created by SharpDevelop.
 * User: Montserrat
 * Date: 19/03/2019
 * Time: 02:34 a. m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace proyecto_part1
{
	partial class PersonajeForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.ImageList imageListPersonajes;
		private System.Windows.Forms.ListView listViewPersonajes;
		private System.Windows.Forms.TextBox nombrePersonaje;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.PictureBox pictureBoxPersonaje;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button Agregar;
		private System.Windows.Forms.Button acceptar;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.ImageList imageListFaces;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PersonajeForm));
			System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem("", 0);
			System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem("", 1);
			System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem("", 2);
			System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem("", 3);
			System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem("", 4);
			System.Windows.Forms.ListViewItem listViewItem6 = new System.Windows.Forms.ListViewItem("", 5);
			System.Windows.Forms.ListViewItem listViewItem7 = new System.Windows.Forms.ListViewItem("", 6);
			this.imageListPersonajes = new System.Windows.Forms.ImageList(this.components);
			this.listViewPersonajes = new System.Windows.Forms.ListView();
			this.imageListFaces = new System.Windows.Forms.ImageList(this.components);
			this.nombrePersonaje = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.pictureBoxPersonaje = new System.Windows.Forms.PictureBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.button3 = new System.Windows.Forms.Button();
			this.Agregar = new System.Windows.Forms.Button();
			this.acceptar = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.panel2 = new System.Windows.Forms.Panel();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxPersonaje)).BeginInit();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// imageListPersonajes
			// 
			this.imageListPersonajes.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListPersonajes.ImageStream")));
			this.imageListPersonajes.TransparentColor = System.Drawing.Color.Transparent;
			this.imageListPersonajes.Images.SetKeyName(0, "personaje01-F01.png");
			this.imageListPersonajes.Images.SetKeyName(1, "fila-1-col-2.png");
			this.imageListPersonajes.Images.SetKeyName(2, "fila-1-col-2-1.png");
			this.imageListPersonajes.Images.SetKeyName(3, "fila-1-col-2.png");
			this.imageListPersonajes.Images.SetKeyName(4, "fila-1-col-2-2.png");
			this.imageListPersonajes.Images.SetKeyName(5, "fila-1-col-2-3.png");
			this.imageListPersonajes.Images.SetKeyName(6, "fila-1-col-2-4.png");
			// 
			// listViewPersonajes
			// 
			this.listViewPersonajes.Activation = System.Windows.Forms.ItemActivation.OneClick;
			this.listViewPersonajes.Alignment = System.Windows.Forms.ListViewAlignment.Left;
			this.listViewPersonajes.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.listViewPersonajes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.listViewPersonajes.Cursor = System.Windows.Forms.Cursors.Hand;
			this.listViewPersonajes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.listViewPersonajes.ForeColor = System.Drawing.SystemColors.Window;
			listViewItem1.Tag = "0";
			listViewItem2.Tag = "1";
			listViewItem3.Tag = "2";
			listViewItem4.Tag = "3";
			listViewItem5.Tag = "4";
			listViewItem6.Tag = "5";
			listViewItem7.Tag = "6";
			this.listViewPersonajes.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
			listViewItem1,
			listViewItem2,
			listViewItem3,
			listViewItem4,
			listViewItem5,
			listViewItem6,
			listViewItem7});
			this.listViewPersonajes.LabelWrap = false;
			this.listViewPersonajes.LargeImageList = this.imageListFaces;
			this.listViewPersonajes.Location = new System.Drawing.Point(13, 44);
			this.listViewPersonajes.Margin = new System.Windows.Forms.Padding(0);
			this.listViewPersonajes.MultiSelect = false;
			this.listViewPersonajes.Name = "listViewPersonajes";
			this.listViewPersonajes.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.listViewPersonajes.Size = new System.Drawing.Size(411, 103);
			this.listViewPersonajes.SmallImageList = this.imageListFaces;
			this.listViewPersonajes.Sorting = System.Windows.Forms.SortOrder.Ascending;
			this.listViewPersonajes.StateImageList = this.imageListFaces;
			this.listViewPersonajes.TabIndex = 0;
			this.listViewPersonajes.UseCompatibleStateImageBehavior = false;
			this.listViewPersonajes.SelectedIndexChanged += new System.EventHandler(this.ListViewPersonajesSelectedIndexChanged);
			// 
			// imageListFaces
			// 
			this.imageListFaces.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListFaces.ImageStream")));
			this.imageListFaces.TransparentColor = System.Drawing.Color.Transparent;
			this.imageListFaces.Images.SetKeyName(0, "M2.png");
			this.imageListFaces.Images.SetKeyName(1, "H3.png");
			this.imageListFaces.Images.SetKeyName(2, "M1.png");
			this.imageListFaces.Images.SetKeyName(3, "M7.png");
			this.imageListFaces.Images.SetKeyName(4, "H1.png");
			this.imageListFaces.Images.SetKeyName(5, "H4.png");
			this.imageListFaces.Images.SetKeyName(6, "H5.png");
			// 
			// nombrePersonaje
			// 
			this.nombrePersonaje.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.nombrePersonaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.nombrePersonaje.Location = new System.Drawing.Point(13, 191);
			this.nombrePersonaje.Name = "nombrePersonaje";
			this.nombrePersonaje.Size = new System.Drawing.Size(292, 31);
			this.nombrePersonaje.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(13, 165);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(211, 23);
			this.label1.TabIndex = 2;
			this.label1.Text = "Nombre del Personaje";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(13, 18);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(211, 23);
			this.label2.TabIndex = 3;
			this.label2.Text = "Elige el personaje";
			// 
			// pictureBoxPersonaje
			// 
			this.pictureBoxPersonaje.Location = new System.Drawing.Point(326, 165);
			this.pictureBoxPersonaje.Margin = new System.Windows.Forms.Padding(0);
			this.pictureBoxPersonaje.Name = "pictureBoxPersonaje";
			this.pictureBoxPersonaje.Size = new System.Drawing.Size(98, 87);
			this.pictureBoxPersonaje.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			this.pictureBoxPersonaje.TabIndex = 4;
			this.pictureBoxPersonaje.TabStop = false;
			this.pictureBoxPersonaje.Click += new System.EventHandler(this.PictureBoxPersonajeClick);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.button3);
			this.panel1.Controls.Add(this.Agregar);
			this.panel1.Controls.Add(this.acceptar);
			this.panel1.Controls.Add(this.listViewPersonajes);
			this.panel1.Controls.Add(this.pictureBoxPersonaje);
			this.panel1.Controls.Add(this.nombrePersonaje);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(453, 339);
			this.panel1.TabIndex = 5;
			// 
			// button3
			// 
			this.button3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
			this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.button3.Location = new System.Drawing.Point(294, 274);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(130, 36);
			this.button3.TabIndex = 7;
			this.button3.Text = "Cancelar";
			this.button3.UseVisualStyleBackColor = false;
			this.button3.Click += new System.EventHandler(this.Button3Click);
			// 
			// Agregar
			// 
			this.Agregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.Agregar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			this.Agregar.Location = new System.Drawing.Point(158, 274);
			this.Agregar.Name = "Agregar";
			this.Agregar.Size = new System.Drawing.Size(130, 36);
			this.Agregar.TabIndex = 6;
			this.Agregar.Text = "Agregar personaje";
			this.Agregar.UseVisualStyleBackColor = true;
			this.Agregar.Click += new System.EventHandler(this.AgregarClick);
			// 
			// acceptar
			// 
			this.acceptar.BackColor = System.Drawing.Color.Transparent;
			this.acceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.acceptar.ForeColor = System.Drawing.Color.Green;
			this.acceptar.Location = new System.Drawing.Point(22, 274);
			this.acceptar.Name = "acceptar";
			this.acceptar.Size = new System.Drawing.Size(130, 36);
			this.acceptar.TabIndex = 5;
			this.acceptar.Text = "Guardar";
			this.acceptar.UseVisualStyleBackColor = false;
			this.acceptar.Click += new System.EventHandler(this.AcceptarClick);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(638, 18);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(100, 23);
			this.label3.TabIndex = 6;
			this.label3.Text = "Pesos de terrenos";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(744, 18);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(30, 23);
			this.label4.TabIndex = 7;
			this.label4.Text = "NA";
			// 
			// panel2
			// 
			this.panel2.AutoScroll = true;
			this.panel2.Location = new System.Drawing.Point(459, 44);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(420, 295);
			this.panel2.TabIndex = 8;
			// 
			// PersonajeForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.ClientSize = new System.Drawing.Size(883, 338);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.panel2);
			this.Name = "PersonajeForm";
			this.Text = "Crea tu personaje";
			this.Load += new System.EventHandler(this.PersonajeFormLoad);
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxPersonaje)).EndInit();
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.ResumeLayout(false);

		}
	}
}
