﻿/*
 * Created by SharpDevelop.
 * User: Montserrat
 * Date: 24/02/2019
 * Time: 12:09 p. m.
 * 
 */
using System;
using System.Windows.Forms;
using System.Diagnostics;

namespace proyecto_part1
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	/// 
	public partial class MainForm : Form
	{
		public MainForm()
		{
			InitializeComponent();
			version.Text = "Parte 02: 1.7.0";
		}
		
		public void BtnCargarMapaClick(object sender, EventArgs e)
		{
			OpenFileDialog vBuscarArchivo = new OpenFileDialog();
			vBuscarArchivo.Filter = "Archivos de Texto|*.txt";
			vBuscarArchivo.Title = "Seleccione el Mapa";
			
			if(vBuscarArchivo.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				try
				{
					using(System.IO.StreamReader lectorArchivo = new System.IO.StreamReader(vBuscarArchivo.FileName))
					{
						String laberinto = lectorArchivo.ReadToEnd();
						lectorArchivo.Close();
						MessageBox.Show(laberinto, "String"); //Fines de pruebas
						
						Parseo labParseado = new Parseo(laberinto);
						
						
						if(labParseado.esLaberintoValido())
						{
							MessageBox.Show("Caracteres Validos");
							labParseado.formarMatriz();
							FormTextura nuevaTextura = new FormTextura(labParseado.matrizDeLaberinto, labParseado.cantidadFilas, labParseado.cantidadColumnas);
							
							//Descomentar si se desea visualizar el contenido de la matriz
							//labParseado.imprimirMatriz();
							this.Hide();
							nuevaTextura.Show();
							
						}
					}
				}
				catch (Exception ex)
				{
					MessageBox.Show("El archivo no pudo ser leido", "Error de lectura");
					MessageBox.Show(ex.Message,"Error");
					Debug.WriteLine("El archivo no pudo ser leido");
					Debug.WriteLine(ex.Message);
				}
			}
			else
			{
				MessageBox.Show("No has seleccionado ningun archivo", "Advertencia");
				Console.WriteLine("El archivo no pudo ser leido");
			}
		}
		void MainFormFormClosing(object sender, FormClosingEventArgs e)
		{
			DialogResult cerrar = MessageBox.Show("¿Realmente quiere salir del programa?", "Cerrar", MessageBoxButtons.YesNo);
			if(cerrar == DialogResult.Yes)
			{
				Application.ExitThread();
			}
			else if(cerrar == DialogResult.No)
			{
				e.Cancel = true;
			}
		}
		void Button1Click(object sender, EventArgs e)
		{
			Application.ExitThread();
		}
	}
}
