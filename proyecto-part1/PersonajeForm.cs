﻿/*
 * Created by SharpDevelop.
 * User: Montserrat
 * Date: 19/03/2019
 * Time: 02:34 a. m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Globalization;

namespace proyecto_part1
{
	/// <summary>
	/// Description of PersonajeForm.
	/// </summary>
	public partial class PersonajeForm : Form
	{
		
		public List<Textura> texturas = new List<Textura>();
		
		public bool existePersonaje;
		
		Personaje personaje;
		public List<Personaje> objPersonaje  = new List<Personaje>();
		
		public PersonajeForm(List<Textura> _texturas)
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			texturas = _texturas;
			
			maquetarListPesos(texturas);
		}

		public void maquetarListPesos(List<Textura> _texturas)
		{
			int y = 10;
			int x = 70;
			for(int i=0; i<_texturas.Count; i++)
			{
				Label idTextura       = new Label();
				Label nombreTextura   = new Label();
				PictureBox imgTextura = new PictureBox();
				TextBox pesoTextura   = new TextBox();
				CheckBox habilitar    = new CheckBox();
				
				imgTextura.Size = new System.Drawing.Size(25,25);
				imgTextura.Location = new Point(x+5,y);
				imgTextura.Image = _texturas[i].imgTextura;
				
				idTextura.Size = new System.Drawing.Size(20, 20);
				idTextura.Location = new Point(x+30, y);
				idTextura.Name = "idTextura"+i;
				idTextura.Text = _texturas[i].idTextura;
				
				nombreTextura.Size = new System.Drawing.Size(50, 20);
				nombreTextura.Location = new Point(x+55, y);
				nombreTextura.Name = "nombreTextura"+i;
				nombreTextura.Text = _texturas[i].nombreTextura;
				
				pesoTextura.Size = new System.Drawing.Size(90, 20);
				pesoTextura.Location = new Point(x+110, y);
				pesoTextura.Name = "pesoTextura"+i;
				
				habilitar.Size = new Size(20, 20);
				habilitar.Name = "habilitar"+i;
				habilitar.Location = new Point(x+220,y);
				
				y += 45;
				
				habilitar.CheckedChanged += new EventHandler(handler_CheckedChanged);
				
				
				panel2.Controls.Add(imgTextura);
				panel2.Controls.Add(idTextura);
				panel2.Controls.Add(nombreTextura);
				panel2.Controls.Add(pesoTextura);
				panel2.Controls.Add(habilitar);
			}

		}
		void ListViewPersonajesSelectedIndexChanged(object sender, EventArgs e)
		{
			foreach(ListViewItem itm in listViewPersonajes.SelectedItems)
			{
				int imgIndex = itm.ImageIndex;
				pictureBoxPersonaje.Image = imageListPersonajes.Images[imgIndex];
				
			}
			
		}
		
		
		void AcceptarClick(object sender, EventArgs e)
		{
			acceptar.Enabled = false;
			this.Hide();
		}
		
		
		void handler_CheckedChanged(object sender, EventArgs e)
		{
			char[] MyChar = {'h','a','b','i','l','r','t'};
			
			CheckBox check = (CheckBox)sender;
			string i = check.Name.TrimStart(MyChar);
			TextBox text   = this.Controls.Find("pesoTextura"+i, true).FirstOrDefault() as TextBox;
			
			if(check.Checked)
			{
				text.Enabled = false;
				text.Text = "-1";
			}
			else
			{
				text.Enabled = true;
				text.Text = "";
			}
		}
		void AgregarClick(object sender, EventArgs e)
		{
			if(objPersonaje.Count <= 4)
			{
				bool valido   = true;
				const String regex = @"(-1)$|^(([0-9]?)+([.][0-9][0-9]?)?$)";
				
				//	(-1)$|^(([0-9]?)+([.][0-9][0-9]?)?$)
				//Posible solución que no funciona
				
				PesoTextura peso;
				List<PesoTextura> objPesoTextura = new List<PesoTextura>();
				
				if(nombrePersonaje.Text == String.Empty)
				{
					MessageBox.Show("No le has puesto nombre a una textura");
					valido = false;
				}
				else if(pictureBoxPersonaje.Image == null)
				{
					MessageBox.Show("No a seleccionado ningun personaje");
					valido = false;
				}
				else
				{
					for(int i = 0; i<texturas.Count; i++)
					{
						TextBox text   = this.Controls.Find("pesoTextura"+i.ToString(), true).FirstOrDefault() as TextBox;
						Label label = this.Controls.Find("idTextura"+i.ToString(), true).FirstOrDefault() as Label;
						
						MatchCollection matches = Regex.Matches(text.Text, regex);
						
						if(text.Text == String.Empty || text.Text == null || text.Text == "")
						{
							MessageBox.Show("No le has puesto peso a una textura");
							valido = false;
						}
						else if(matches.Count == 0)
						{
							MessageBox.Show("El peso no es un número");
							valido = false;
						}
						if(valido)
						{
							peso = new PesoTextura(label.Text, float.Parse(text.Text, CultureInfo.InvariantCulture.NumberFormat));
							objPesoTextura.Add(peso);
						}
						else
							break;
					}
				}
				if(valido)
				{
					personaje = new Personaje(nombrePersonaje.Text, pictureBoxPersonaje.Image, objPesoTextura);
					objPersonaje.Add(personaje);
					
					nombrePersonaje.Text = null;
					pictureBoxPersonaje.Image = null;
					
					for(int i = 0; i<texturas.Count; i++)
					{
						TextBox text   = this.Controls.Find("pesoTextura"+i.ToString(), true).FirstOrDefault() as TextBox;
						text.Text = null;
						CheckBox cb = this.Controls.Find("habilitar"+i.ToString(), true).FirstOrDefault() as CheckBox;
						cb.Checked = false;
					}
					
					MessageBox.Show("Se añadio correctamente el personaje");
					existePersonaje = true;
					acceptar.Enabled = true;
					
				}
			}
			else
			{
				MessageBox.Show("Se ha llegado al limite de personajes");
			}
			
		}
		void Button3Click(object sender, EventArgs e)
		{
			nombrePersonaje.Text = null;
			pictureBoxPersonaje.Image = null;
			
			for(int i = 0; i<texturas.Count; i++)
			{
				TextBox text   = this.Controls.Find("pesoTextura"+i.ToString(), true).FirstOrDefault() as TextBox;
				text.Text = null;
				CheckBox cb = this.Controls.Find("habilitar"+i.ToString(), true).FirstOrDefault() as CheckBox;
				cb.Checked = false;
			}
			this.Hide();
		}
		void PersonajeFormLoad(object sender, EventArgs e)
		{
			acceptar.Enabled = false;
		}
		void PictureBoxPersonajeClick(object sender, EventArgs e)
		{
	
		}
	}
}
