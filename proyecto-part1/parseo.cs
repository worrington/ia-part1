﻿/*
 * Created by SharpDevelop.
 * User: CesarArleyOE
 * Date: 26/02/2019
 * Time: 11:27 a. m.
 * 
 */
using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;



namespace proyecto_part1
{
	/// <summary>
	/// Esta clase tiene los metodos necesarios
	/// para realizar el parseo de una cadena
	/// que contenga el laberinto.
	/// </summary>
	public class Parseo
	{
		private readonly String laberinto;
		public  int cantidadFilas = 0;
		public  int cantidadColumnas = 0;
		
		MatchCollection matchesTokens;
		
		public int[,] matrizDeLaberinto;
		
		public Parseo(String laberintoInput)
		{
			laberinto = laberintoInput;
		}
		
		public bool esLaberintoValido()
		{
			// Contiene el total de las validaciones
			
			if(sonCaracteresValidos() && noHayVacios() && sonFilasConsistentes())
			{
				const String regexTok = @"[0-9]+";
				matchesTokens = Regex.Matches(laberinto,  regexTok);
				return true;
			}
			
			return false;
		}
		
		/*
		 * 	Se verifica que no existan caracteres diferentes a comas, saltos de linea y numeros enteros positivos
		 * 	Regresa un verdadero si todo es correcto
		 *  Muestra una ventana si no lo es y retorna false
		 */
		
		private bool sonCaracteresValidos()
		{
			const String regex = @"[^0-9\,\n\r]";
			MatchCollection matches = Regex.Matches(laberinto, regex);
			
			if(matches.Count == 0)
			{
				return true;
			}
			else
			{
				foreach (Match match in matches)
					MessageBox.Show("Existe el caracter: " + match.Value +
					                ", el cual es invalido. Se encuentra en la posicion: "
					                + match.Index +". Por favor corrige el laberinto o selecciona otro.", "Error");
				return false;
			}
		}
		
		/*
		 * 	Se verifica que no existan celdas que no contengan codigo
		 * 	Regresa un verdadero si todo es correcto
		 *  Muestra una ventana si no lo es y retorna false
		 */
		
		private bool noHayVacios()
		{
			const String regex = @"(\,){2,}|(\,\n|\,\r)";
			MatchCollection matches = Regex.Matches(laberinto, regex);
			if(matches.Count == 0)
			{
				return true;
			}
			else
			{
				foreach (Match match in matches)
					MessageBox.Show("Existe el caracter: " + match.Value +
					                ", el cual es invalido. Se encuentra en la posicion: "
					                + match.Index +". Por favor corrige el laberinto o selecciona otro.");
				return false;
			}
			
		}
		
		/*
		 * 	Se verifica que todas las filas tengan la misma longitud
		 * 	Regresa un verdadero si son consistentes
		 *  Muestra una ventana si no lo son y retorna false
		 */
		
		private bool sonFilasConsistentes()
		{
			const String regex = @"[0-9\,]+";
			MatchCollection matches = Regex.Matches(laberinto, regex);
			
			const String regexTok = @"[0-9]+";
			
			int actual = 0;
			int anterior = Regex.Matches(matches[0].ToString(),  regexTok).Count;
			
			
			// Comparamos la longitud de la fila anterior con la actual
			foreach (Match match in matches)
			{
				
				MatchCollection matchesTok = Regex.Matches(match.ToString(),  regexTok);
				
				actual = matchesTok.Count;
				if(actual == anterior)
				{
					anterior = actual;
					
				}
				else
				{
					MessageBox.Show("Las filas no son consistentes en tu laberinto.", "Error");
					return false;
				}
			}
			cantidadColumnas = actual;
			cantidadFilas = matches.Count;
			
			matrizDeLaberinto = new int[cantidadFilas,cantidadColumnas];
			return true;
		}
		
		/*
		 * 	Se crea la matriz de enteros que contiene los diferentes
		 * 	códigos de texturas.
		 * 	La matriz se guarda en el atributo matrizDeLaberinto
		 */
		
		public void formarMatriz()
		{
			int token = 0;

			for(int fil = 0; fil < cantidadFilas; fil++)
			{
				for(int col = 0; col < cantidadColumnas; col++)
				{
					matrizDeLaberinto[fil, col] = Convert.ToInt32(matchesTokens[token].Value.ToString());
					token++;
				}
			}
			
		}
		
		/*
		 *	Este metodo solo se usa para fines de debugeo
		 *	Solo imprime la matriz creada usando MessageBox
		 */
		
		public void imprimirMatriz()
		{
			for(int fil = 0; fil < cantidadFilas; fil++)
			{
				for(int col = 0; col < cantidadColumnas; col++)
				{
					MessageBox.Show(matrizDeLaberinto[fil, col].ToString(), "Fila: " + fil.ToString()
					                + ", Columna: " + col.ToString());
				}
			}
		}
		
	}
}
