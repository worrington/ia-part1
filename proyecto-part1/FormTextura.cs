﻿/*
 * Created by SharpDevelop.
 * User: Montserrat
 * Date: 09/03/2019
 * Time: 11:54 p. m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;


namespace proyecto_part1
{
	/// <summary>
	/// Description of FormTextura.
	/// </summary>
	///

	public partial class FormTextura : Form
	{
		private readonly int[,] caracteresLaberinto;
		private int y = 20;
		private int caracter = 0;
		private int conteo = 0;
		private int nColumnas = 0;
		private int nFilas = 0;
		
		List<int> idTextura = new List<int>();
		Textura  textura;
		
		public List<Textura> listaTexturas = new List<Textura>();
		
		public FormTextura(int[,] laberinto, int f, int c)
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			
			caracteresLaberinto = laberinto;
			nColumnas = c;
			nFilas = f;
			InitializeComponent();
			generarLista(f, c);
			maquetar();
		}
		
		
		public void generarLista(int f, int c)
		{
			for(int i=0; i<f; i++)
			{
				for(int j=0; j<c; j++)
				{
					if(!idTextura.Exists(x => x ==(caracteresLaberinto[i,j])))
					{
						idTextura.Add(caracteresLaberinto[i,j]);
						caracter+=1;
					}
				}
			}
		}
		
		
		public void maquetar()
		{
			
			String[] colores = Enum.GetNames(typeof(System.Drawing.KnownColor));
			foreach(int id in idTextura)
			{
				Label idTemp = new Label();
				TextBox nombreTemp = new TextBox();
				ComboBox textTemp  = new ComboBox();
				
				
				idTemp.Size = new System.Drawing.Size(70, 50);
				idTemp.Location = new Point(50, y);
				idTemp.Name = "idTextura"+conteo;
				idTemp.Text = id.ToString();

				nombreTemp.Size = new Size(220,50);
				nombreTemp.Location = new Point(150, y);
				nombreTemp.Name = "nombreTextura"+ conteo;

				textTemp.Size = new Size(55,50);
				textTemp.Location = new Point(400, y);
				textTemp.Name = "imgTextura"+ conteo;
				textTemp.DropDownHeight = 100;
				textTemp.DropDownStyle = ComboBoxStyle.DropDownList;
				textTemp.DrawMode = DrawMode.OwnerDrawFixed;
				
				
				textTemp.Items.AddRange(
					Enumerable.Repeat<string>(conteo.ToString(), listTexturas.Images.Count).ToArray()
				);
				
				nombreTemp.Leave += new EventHandler(handles_nombreTempChanged);
				textTemp.DrawItem += new DrawItemEventHandler(handler_DrawItem);
				textTemp.SelectedIndexChanged+= new EventHandler(handler_SelectedIndexChanged);
				
				y += 65;
				conteo++;
				
				panel1.Controls.Add(idTemp);
				panel1.Controls.Add(nombreTemp);
				panel1.Controls.Add(textTemp);
			}
			
		}
		
		public void handles_nombreTempChanged(object sender, EventArgs e){
			try
			{
				List<string> nombres = new List<string>();
				
				
				for(int i = 0; i<conteo; i++)
				{
					TextBox text = this.Controls.Find("nombreTextura"+i.ToString(), true).FirstOrDefault() as TextBox;
					
					if(text.Text != String.Empty)
					{
						if(nombres.Contains(text.Text))
						{
							MessageBox.Show(text.Text + ": este nombre ya existe \nPor favor cambialo", "Error");
							text.Text = "";
						}
						else
						{
							nombres.Add(text.Text);
						}					
					}
					
					
					
				}
			}
			catch(Exception ex)
			{
				Debug.WriteLine(ex.Message);
			}
			
		}
		
		public void handler_DrawItem(object sender, DrawItemEventArgs e)
		{
			e.DrawBackground();
			e.DrawFocusRectangle();
			if (e.Index >= 0)
			{
				if (e.Index < listTexturas.Images.Count)
				{
					Image img = new Bitmap(listTexturas.Images[e.Index], new Size(32, 32));
					e.Graphics.DrawImage(img, new PointF(e.Bounds.Left, e.Bounds.Top));
				}
				
			}
		}
		
		public void handler_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				List<int> texturas = new List<int>();
				
				for(int i = 0; i<conteo; i++)
				{
				
					ComboBox combo = this.Controls.Find("imgTextura"+i.ToString(), true).FirstOrDefault() as ComboBox;
					
					if(combo.SelectedItem != null || combo.SelectedIndex != -1)
					{
						if(texturas.Contains(combo.SelectedIndex))
						{
							MessageBox.Show("Esta textura ya esta seleccionada \nPor favor cambiala", "Error");
							combo.SelectedItem = null;
						}
						else
						{
							texturas.Add(combo.SelectedIndex);
						}
					}
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
			
		}
		
		void AceptarClick(object sender, EventArgs e)
		{
			
			bool valido = true;
			
			for(int i = 0; i<conteo; i++)
			{
				TextBox text   = this.Controls.Find("nombreTextura"+i.ToString(), true).FirstOrDefault() as TextBox;
				ComboBox combo = this.Controls.Find("imgTextura"+i.ToString(), true).FirstOrDefault() as ComboBox;
				Label label = this.Controls.Find("idTextura"+i.ToString(), true).FirstOrDefault() as Label;
				
				if(text.Text == String.Empty)
				{
					MessageBox.Show("No le has puesto nombre a una textura");
					valido = false;
					Debug.WriteLine("Nombre: " + valido);
				}
				else if(combo.SelectedItem == null || combo.SelectedIndex == -1)
				{
					MessageBox.Show("No le has seleccionado una Textura");
					valido = false;
					
				}
				if(valido)
				{
					Debug.WriteLine("Valido: " + valido);
					textura = new Textura(label.Text, text.Text, listTexturas.Images[combo.SelectedIndex]);
					
					listaTexturas.Add(textura);
				}
				else
					break;
			}
			if(valido)
			{
				Laberinto FormLaberinto = new Laberinto(nFilas, nColumnas, listaTexturas, caracteresLaberinto);
				this.Hide();
				FormLaberinto.Show();
			}
		}
		void FormTexturaFormClosing(object sender, FormClosingEventArgs e)
		{
			DialogResult cerrar = MessageBox.Show("¿Realmente quiere salir del programa?", "Cerrar", MessageBoxButtons.YesNo);
			if(cerrar == DialogResult.Yes)
			{
				Application.ExitThread();
			}
			else if(cerrar == DialogResult.No)
			{
				e.Cancel = true;
			}
		}
	}
}