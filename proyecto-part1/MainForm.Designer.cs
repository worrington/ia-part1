﻿/*
 * Created by SharpDevelop.
 * User: Montserrat
 * Date: 24/02/2019
 * Time: 12:09 p. m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace proyecto_part1
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.Button btnCargarMapa;
		private System.Windows.Forms.Label lblTitulo;
		private System.Windows.Forms.Label version;
		private System.Windows.Forms.Button button1;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnCargarMapa = new System.Windows.Forms.Button();
			this.lblTitulo = new System.Windows.Forms.Label();
			this.version = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btnCargarMapa
			// 
			this.btnCargarMapa.BackColor = System.Drawing.Color.Green;
			this.btnCargarMapa.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
			this.btnCargarMapa.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnCargarMapa.ForeColor = System.Drawing.Color.White;
			this.btnCargarMapa.Location = new System.Drawing.Point(111, 212);
			this.btnCargarMapa.Name = "btnCargarMapa";
			this.btnCargarMapa.Size = new System.Drawing.Size(350, 72);
			this.btnCargarMapa.TabIndex = 0;
			this.btnCargarMapa.Text = "Cargar Mapa";
			this.btnCargarMapa.UseVisualStyleBackColor = false;
			this.btnCargarMapa.Click += new System.EventHandler(this.BtnCargarMapaClick);
			// 
			// lblTitulo
			// 
			this.lblTitulo.BackColor = System.Drawing.Color.Transparent;
			this.lblTitulo.Font = new System.Drawing.Font("Microsoft YaHei UI", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblTitulo.ForeColor = System.Drawing.Color.White;
			this.lblTitulo.Location = new System.Drawing.Point(39, 25);
			this.lblTitulo.Name = "lblTitulo";
			this.lblTitulo.Size = new System.Drawing.Size(517, 138);
			this.lblTitulo.TabIndex = 1;
			this.lblTitulo.Text = "Laberinto Inteligente";
			this.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// version
			// 
			this.version.BackColor = System.Drawing.Color.Transparent;
			this.version.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.version.ForeColor = System.Drawing.Color.OrangeRed;
			this.version.Location = new System.Drawing.Point(393, 425);
			this.version.Name = "version";
			this.version.Size = new System.Drawing.Size(175, 23);
			this.version.TabIndex = 2;
			this.version.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.Red;
			this.button1.Font = new System.Drawing.Font("Nirmala UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button1.ForeColor = System.Drawing.Color.White;
			this.button1.Location = new System.Drawing.Point(212, 300);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(162, 59);
			this.button1.TabIndex = 3;
			this.button1.Text = "Salir";
			this.button1.UseVisualStyleBackColor = false;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImage = global::proyecto_part1.Resource1.fondo;
			this.ClientSize = new System.Drawing.Size(580, 457);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.version);
			this.Controls.Add(this.lblTitulo);
			this.Controls.Add(this.btnCargarMapa);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Proyecto de Inteligencia Artificial I";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainFormFormClosing);
			this.ResumeLayout(false);

		}
	}
}
