﻿/*
 * Created by SharpDevelop.
 * User: Montserrat
 * Date: 09/03/2019
 * Time: 11:54 p. m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
handlerComunText */
namespace proyecto_part1
{
	partial class FormTextura
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ImageList listTexturas;
		private System.Windows.Forms.Button aceptar;
		private System.Windows.Forms.Panel panel1;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormTextura));
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.listTexturas = new System.Windows.Forms.ImageList(this.components);
			this.aceptar = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
			this.label1.Location = new System.Drawing.Point(15, 28);
			this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(246, 52);
			this.label1.TabIndex = 0;
			this.label1.Text = "Identificador";
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
			this.label2.Location = new System.Drawing.Point(148, 28);
			this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(246, 52);
			this.label2.TabIndex = 1;
			this.label2.Text = "Nombre de la textura";
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
			this.label3.Location = new System.Drawing.Point(398, 28);
			this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(246, 52);
			this.label3.TabIndex = 2;
			this.label3.Text = "Textura";
			// 
			// listTexturas
			// 
			this.listTexturas.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("listTexturas.ImageStream")));
			this.listTexturas.TransparentColor = System.Drawing.Color.Transparent;
			this.listTexturas.Images.SetKeyName(0, "agua-01.png");
			this.listTexturas.Images.SetKeyName(1, "agua-02.png");
			this.listTexturas.Images.SetKeyName(2, "agua-03.png");
			this.listTexturas.Images.SetKeyName(3, "arena-01.png");
			this.listTexturas.Images.SetKeyName(4, "arena-02.png");
			this.listTexturas.Images.SetKeyName(5, "arena-03.png");
			this.listTexturas.Images.SetKeyName(6, "arena-04.png");
			this.listTexturas.Images.SetKeyName(7, "arena-05.png");
			this.listTexturas.Images.SetKeyName(8, "bosque.png");
			this.listTexturas.Images.SetKeyName(9, "bosque-02.png");
			this.listTexturas.Images.SetKeyName(10, "camino-01.png");
			this.listTexturas.Images.SetKeyName(11, "camino-02.png");
			this.listTexturas.Images.SetKeyName(12, "camino-03.png");
			this.listTexturas.Images.SetKeyName(13, "camino-04.png");
			this.listTexturas.Images.SetKeyName(14, "camino-05.png");
			this.listTexturas.Images.SetKeyName(15, "camino-06.png");
			this.listTexturas.Images.SetKeyName(16, "camino-07.png");
			this.listTexturas.Images.SetKeyName(17, "camino-08.png");
			this.listTexturas.Images.SetKeyName(18, "camino-09.png");
			this.listTexturas.Images.SetKeyName(19, "camino-10.png");
			this.listTexturas.Images.SetKeyName(20, "camino-11.png");
			this.listTexturas.Images.SetKeyName(21, "hielo.png");
			this.listTexturas.Images.SetKeyName(22, "hielo-02.png");
			this.listTexturas.Images.SetKeyName(23, "iceberg.png");
			this.listTexturas.Images.SetKeyName(24, "lava-01.png");
			this.listTexturas.Images.SetKeyName(25, "lava-02.png");
			this.listTexturas.Images.SetKeyName(26, "lava-03.png");
			this.listTexturas.Images.SetKeyName(27, "madera-01.png");
			this.listTexturas.Images.SetKeyName(28, "madera-02.png");
			this.listTexturas.Images.SetKeyName(29, "montania-01.png");
			this.listTexturas.Images.SetKeyName(30, "montania-02.png");
			this.listTexturas.Images.SetKeyName(31, "montania-03.png");
			this.listTexturas.Images.SetKeyName(32, "montania-04.png");
			this.listTexturas.Images.SetKeyName(33, "nieve.png");
			this.listTexturas.Images.SetKeyName(34, "palmeras.png");
			this.listTexturas.Images.SetKeyName(35, "pantano-01.png");
			this.listTexturas.Images.SetKeyName(36, "pantano-02.png");
			this.listTexturas.Images.SetKeyName(37, "pareded-01.png");
			this.listTexturas.Images.SetKeyName(38, "pareded-02.png");
			this.listTexturas.Images.SetKeyName(39, "pareded-03.png");
			this.listTexturas.Images.SetKeyName(40, "pareded-04.png");
			this.listTexturas.Images.SetKeyName(41, "pareded-05.png");
			this.listTexturas.Images.SetKeyName(42, "pareded-06.png");
			this.listTexturas.Images.SetKeyName(43, "pareded-07.png");
			this.listTexturas.Images.SetKeyName(44, "pareded-08.png");
			this.listTexturas.Images.SetKeyName(45, "pasto-01.png");
			this.listTexturas.Images.SetKeyName(46, "pasto-02.png");
			this.listTexturas.Images.SetKeyName(47, "pasto-03.png");
			this.listTexturas.Images.SetKeyName(48, "pasto-05.png");
			this.listTexturas.Images.SetKeyName(49, "rocas-01.png");
			// 
			// aceptar
			// 
			this.aceptar.Location = new System.Drawing.Point(518, 79);
			this.aceptar.Name = "aceptar";
			this.aceptar.Size = new System.Drawing.Size(125, 56);
			this.aceptar.TabIndex = 3;
			this.aceptar.Text = "Aceptar";
			this.aceptar.UseVisualStyleBackColor = true;
			this.aceptar.Click += new System.EventHandler(this.AceptarClick);
			// 
			// panel1
			// 
			this.panel1.AutoScroll = true;
			this.panel1.Location = new System.Drawing.Point(0, 60);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(495, 248);
			this.panel1.TabIndex = 5;
			// 
			// FormTextura
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(659, 319);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.aceptar);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
			this.Margin = new System.Windows.Forms.Padding(6);
			this.Name = "FormTextura";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "FormTextura";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormTexturaFormClosing);
			this.ResumeLayout(false);

		}
	
	}
	
}
	
