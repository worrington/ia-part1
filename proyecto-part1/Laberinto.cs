﻿/*
 * Created by SharpDevelop.
 * User: CesarArleyOE
 * Date: 16/03/2019
 * Time: 04:16 p. m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace proyecto_part1
{
	
	/// <summary>
	/// Description of Laberinto.
	/// </summary>
	/// 
	public partial class Laberinto : Form
	{
		int nFilas;
		int nColumnas;

		float altoCelda;
		float largoCelda;
		RectangleF[] matriz;
		PointF botonDerecho;
		
		bool banInicialColocada = false;
		Celda posicionInicial;
		bool banFinalColocada = false;
		Celda posicionFinal;
		bool personajeColocado = false;
		Personaje personajeActivo;
		
		List<Textura> listaTexturas;
		
		PersonajeForm crearPersonajes;
		List<Personaje> listaPersonajes;
		
		int numVisita = 1;
		
		int[,] laberinto;
		Celda[,] matrizCeldas;
		RectangleF posicionActualPersonaje;
		
		
		List<Celda> visitados = new List<Celda>();
		
		
		
		public Laberinto(int numFilas, int numColumnas, List<Textura> _listaTexturas, int[,] _laberinto)
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			this.nFilas = numFilas;
			this.nColumnas = numColumnas;
			this.listaTexturas = _listaTexturas;
			
			altoCelda = (float)(this.pictureBox1.Size.Height - 1) / (this.nFilas + 1);
			largoCelda = (float)(this.pictureBox1.Size.Width - 1) / (this.nColumnas + 1);
			
			matriz = new RectangleF[(int)this.altoCelda * (int)this.largoCelda];
			
			this.laberinto = _laberinto;
			matrizCeldas = new Celda[numFilas, numColumnas];
			
			crearPersonajes = new PersonajeForm(listaTexturas);
		}
		
		
		void SalirToolStripMenuItemClick(object sender, EventArgs e)
		{
			
			DialogResult cerrar = MessageBox.Show("¿Realmente quiere salir del programa?", "Cerrar", MessageBoxButtons.YesNo);
			if (cerrar == DialogResult.Yes) {
				Application.ExitThread();
			}
		}
		
		/**
		 * Función encargada de dibujar el laberinto
		 */
		
		void PictureBox1Paint(object sender, PaintEventArgs e)
		{
			Graphics malla = e.Graphics;
			
			
			generarMatriz();
			dibujarIndices(malla);
			dibujarMalla(malla);
			generarMatrizCeldas(malla);
			recargarBanderas(malla);
			cargarPersonaje(malla, posicionActualPersonaje);
			
			
		}
		
		/**
		 * Esta función se encarga de dibujar y
		 * refrescar la malla de m*n
		 */
		
		void generarMatriz()
		{
			float x = 0;
			float y = 0;
			
			int rec = 0;
			for (int i = 0; i <= nFilas; i++) {
				for (int j = 0; j <= nColumnas; j++) {
					
					matriz[rec] = new RectangleF(x, y, largoCelda, altoCelda);
					
					rec++;
					
					x += largoCelda;
				}
				x = 0;
				y += altoCelda;
			}
		}
		
		void generarMatrizCeldas(Graphics malla)
		{
			float x = largoCelda;
			float y = altoCelda;

			for (int i = 0; i < nFilas; i++) {
				for (int j = 0; j < nColumnas; j++) {
					
					matrizCeldas[i, j] = new Celda(i + 1, j + 1, new RectangleF(x, y, largoCelda, altoCelda), buscarTextura(i, j));
					
					cargarTextura(matrizCeldas[i, j], malla);
					
					x += largoCelda;
				}
				x = largoCelda;
				y += altoCelda;
			}
		}
		
		Textura buscarTextura(int fila, int columna)
		{
			for (int i = 0; i < listaTexturas.Count; i++) {
				if (laberinto[fila, columna].ToString() == listaTexturas[i].idTextura) {
					return listaTexturas[i];
				}
			}
			return null;
		}
		
		void dibujarMalla(Graphics malla)
		{
			Pen lapiz = new Pen(Color.Gray, 3);
			malla.DrawRectangles(lapiz, matriz);
		}
		
		void dibujarIndices(Graphics malla)
		{
			const string indicesColumnas = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			float x = 0;
			float y = 0;
			
			float medioX = ((float)largoCelda) / 5;
			float medioY = ((float)altoCelda) / 5;
			
			float emSize = (float)largoCelda * 0.5f;
			
			var centrado = new StringFormat();
			centrado.LineAlignment = StringAlignment.Center;
			centrado.Alignment = StringAlignment.Center;
			
			for (int i = 0; i <= nColumnas; i++) {
				malla.FillRectangle(new SolidBrush(Color.Black), x, y, largoCelda, altoCelda);
				x += largoCelda;
			}
			
			for (int i = 0; i < nColumnas; i++) {
				medioX += largoCelda;
				malla.DrawString(indicesColumnas.Substring(i, 1), new Font(FontFamily.GenericSansSerif, emSize, FontStyle.Bold, GraphicsUnit.Pixel),
				                 new SolidBrush(Color.White), posicionRectangulo(new PointF(medioX, medioY)), centrado);
			}
			x = 0;
			for (int i = 0; i <= nFilas; i++) {
				malla.FillRectangle(new SolidBrush(Color.Black), x, y, largoCelda, altoCelda);
				y += altoCelda;
			}
			medioX = (float)largoCelda * 0.5f;
			for (int i = 0; i < nFilas; i++) {
				medioY += altoCelda;
				malla.DrawString((i + 1).ToString(), new Font(FontFamily.GenericSansSerif, emSize, FontStyle.Bold, GraphicsUnit.Pixel),
				                 new SolidBrush(Color.White), posicionRectangulo(new PointF(medioX, medioY)), centrado);
			}
		}
		
		void PictureBox1MouseClick(object sender, MouseEventArgs e)
		{
			try
			{
				switch (e.Button) {
					case MouseButtons.Right:
						{
							botonDerecho = new PointF(e.Location.X, e.Location.Y);
							if ((botonDerecho.X < largoCelda * (nColumnas + 1) && botonDerecho.Y < altoCelda)) {
								Debug.WriteLine("Indices columnas");
								colocarPuntoDePartidaAquíToolStripMenuItem.Enabled = false;
								colocarMetaAquíToolStripMenuItem.Enabled = false;
								verPropiedadesToolStripMenuItem.Enabled = false;
							} else if ((botonDerecho.X < largoCelda && botonDerecho.Y < altoCelda * (nFilas + 1))) {
								Debug.WriteLine("Indices filas");
								colocarPuntoDePartidaAquíToolStripMenuItem.Enabled = false;
								colocarMetaAquíToolStripMenuItem.Enabled = false;
								verPropiedadesToolStripMenuItem.Enabled = false;
							} else if(!crearPersonajes.existePersonaje){
								Debug.WriteLine("Personaje no creado");
								colocarPuntoDePartidaAquíToolStripMenuItem.Enabled = false;
								colocarMetaAquíToolStripMenuItem.Enabled = false;
								verPropiedadesToolStripMenuItem.Enabled = true;
							}
							else if(buscarPeso(posicionCelda(botonDerecho).textura.idTextura) < 0 ) {
								colocarPuntoDePartidaAquíToolStripMenuItem.Enabled = false;
								colocarMetaAquíToolStripMenuItem.Enabled = false;
								verPropiedadesToolStripMenuItem.Enabled = true;
								
							} else {
								colocarPuntoDePartidaAquíToolStripMenuItem.Enabled = true;
								colocarMetaAquíToolStripMenuItem.Enabled = true;
								verPropiedadesToolStripMenuItem.Enabled = true;
							}
							
							contextMenuStrip1.Show(pictureBox1, new Point(e.Location.X, e.Location.Y));
							
						}
						break;
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show("Debes seleccionar primero un personaje");
				Debug.WriteLine(ex.Message);
			}
			
		}
		
		void dibujarCeldasVecinas(PointF actual)
		{
			try
			{
				Graphics mapa = pictureBox1.CreateGraphics();
				Celda izquierda = new Celda();
				Celda derecha  = new Celda();
				Celda arriba  = new Celda();
				Celda abajo  = new Celda();
			
				if(posicionCelda(new PointF(actual.X-largoCelda+1,actual.Y+1)) != null)
					izquierda = posicionCelda(new PointF(actual.X-largoCelda+1,actual.Y+1));
				
				if(posicionCelda(new PointF(actual.X+largoCelda+1,actual.Y+1)) != null)
					derecha = posicionCelda(new PointF(actual.X+largoCelda+1,actual.Y+1));
				
				if(posicionCelda(new PointF(actual.X+1,actual.Y-altoCelda+1)) != null)
					arriba = posicionCelda(new PointF(actual.X+1,actual.Y-altoCelda+1));
				
				if(posicionCelda(new PointF(actual.X+1,actual.Y+altoCelda+1)) != null)
					abajo = posicionCelda(new PointF(actual.X+1,actual.Y+altoCelda+1));
				
				
				// Corregir sobreposicion de banderas
				
				if(izquierda != null && !izquierda.estaExpandido)
				{
					izquierda.estaExpandido = true;
					cargarTextura(izquierda,mapa);
					recargarBanderas(mapa);
				}
					
				if(derecha != null && !derecha.estaExpandido)
				{
					derecha.estaExpandido = true;
					cargarTextura(derecha,mapa);
					recargarBanderas(mapa);
				}
				
				if(arriba != null && !arriba.estaExpandido)
				{
					arriba.estaExpandido = true;
					cargarTextura(arriba,mapa);
					recargarBanderas(mapa);
				}
				
				if(abajo != null && !abajo.estaExpandido)
				{
					abajo.estaExpandido = true;
					cargarTextura(abajo,mapa);
					recargarBanderas(mapa);
				}
			}
			catch(Exception e)
			{
				Debug.WriteLine(e.Message);
			}
		}
		
		void ColocarPuntoDePartidaAquíToolStripMenuItemClick(object sender, EventArgs e)
		{
			try {
				if (!banInicialColocada) {
					Graphics banderaInicio = pictureBox1.CreateGraphics();
					Image banInicio = Resource1.bandera_inicio;
					posicionCelda(botonDerecho).estaExpandido = true;
					cargarTextura(posicionCelda(botonDerecho),banderaInicio);
					dibujarCeldasVecinas(botonDerecho);
					banderaInicio.DrawImage(banInicio, posicionRectangulo(botonDerecho));
					banInicialColocada = true;
					posicionInicial = posicionCelda(botonDerecho);
					visitados.Add(posicionInicial);
					if (banFinalColocada) {
						posicionActualPersonaje = posicionInicial.posicion;
						dibujarVisitas(banderaInicio);
						personajeColocado = true;
						cargarPersonaje(banderaInicio, posicionInicial.posicion);
					}

				} else {
					Graphics banderaInicio = pictureBox1.CreateGraphics();
					enmascararMapa();
					posicionCelda(botonDerecho).estaExpandido = true;
					cargarTextura(posicionInicial, banderaInicio);
					cargarTextura(posicionCelda(botonDerecho),banderaInicio);
					dibujarCeldasVecinas(botonDerecho);
					Image banInicio = Resource1.bandera_inicio;
					banderaInicio.DrawImage(banInicio, posicionRectangulo(botonDerecho));
					banInicialColocada = true;
					posicionInicial = posicionCelda(botonDerecho);
					cargarTextura(posicionCelda(botonDerecho),banderaInicio);
					posicionActualPersonaje = posicionInicial.posicion;
					
					numVisita = 1;
					limpiarVisitas(banderaInicio);
					visitados.Add(posicionInicial);
					if (banFinalColocada) {
						posicionActualPersonaje = posicionInicial.posicion;
						personajeColocado = true;
						recargarBanderas(banderaInicio);
						cargarPersonaje(banderaInicio, posicionInicial.posicion);
						dibujarVisitas(banderaInicio);
					}
				}
				
			} catch (Exception ex) {
				Debug.WriteLine("Fuera de limite");
				Debug.WriteLine(ex.Message);
			}
		}
		
		void enmascararMapa()
		{
			try
			{
				Graphics mapa = pictureBox1.CreateGraphics();
				for(int i = 0; i < nFilas; i++)
				{
					for(int j = 0; j < nColumnas; j++)
					{
						matrizCeldas[i,j].estaExpandido = false;
						mapa.FillRectangle(new SolidBrush(Color.DimGray), matrizCeldas[i,j].posicion.X, 
						                   matrizCeldas[i,j].posicion.Y, largoCelda, altoCelda);
					}
				}
				dibujarMalla(mapa);
			}
			catch(Exception e)
			{
				Debug.WriteLine(e.Message);
			}
		}

		
		void ColocarMetaAquíToolStripMenuItemClick(object sender, EventArgs e)
		{
			try {
				if (!banFinalColocada) {
					Graphics banderaFinal = pictureBox1.CreateGraphics();
					Image banFinal = Resource1.bandera_meta;
					banderaFinal.DrawImage(banFinal, posicionRectangulo(botonDerecho));
					banFinalColocada = true;
					posicionFinal = posicionCelda(botonDerecho);
					
					if (banInicialColocada) {
						posicionActualPersonaje = posicionInicial.posicion;
						personajeColocado = true;
						dibujarVisitas(banderaFinal);
						cargarPersonaje(banderaFinal, posicionActualPersonaje);
					}
					
					
				} else {
					enmascararMapa();
					Graphics banderaFinal = pictureBox1.CreateGraphics();
					cargarTextura(posicionFinal, banderaFinal);
					Image banFinal = Resource1.bandera_meta;
					banderaFinal.DrawImage(banFinal, posicionRectangulo(botonDerecho));
					banFinalColocada = true;
					posicionFinal = posicionCelda(botonDerecho);
					numVisita = 1;
					limpiarVisitas(banderaFinal);
					
					posicionInicial.estaExpandido = true;
					cargarTextura(posicionInicial, banderaFinal);
					recargarBanderas(banderaFinal);
					dibujarCeldasVecinas(new PointF(posicionInicial.posicion.X, posicionInicial.posicion.Y));
					
					visitados.Add(posicionInicial);
					posicionActualPersonaje = posicionInicial.posicion;
					if (banInicialColocada) {
						personajeColocado = true;
						
						recargarBanderas(banderaFinal);
						cargarPersonaje(banderaFinal, posicionInicial.posicion);
						dibujarVisitas(banderaFinal);
					}
					
				}
				
			} catch (Exception ex) {
				Debug.WriteLine("Fuera de limite");
				Debug.WriteLine(ex.Message);
			}
		}
		
		/* Esta función regresa una celda dependiendo
		 * de la posición el puntoF que se le ingrese
		 */
		Celda posicionCelda(PointF clickDerecho)
		{
			try {
				for (int i = 0; i < nFilas; i++) {
					for (int j = 0; j < nColumnas; j++) {
						if (matrizCeldas[i, j].posicion.Contains(clickDerecho)) {
							return matrizCeldas[i, j];
						}
					}
					
				}
				return null;
				
			} catch (Exception e) {
				Debug.WriteLine("No existe la celda");
				Debug.WriteLine(e);
				return null;
			}
		}
		
		RectangleF posicionRectangulo(PointF clickDerecho)
		{
			for (int i = 0; i < matriz.Length; i++) {
				if (matriz[i].Contains(clickDerecho)) {
					return matriz[i];
				}
			}
			Debug.WriteLine("No existe el rectangulo");
			return new RectangleF(0, 0, 0, 0);
		}
		
		void cargarTextura(Celda celdaPintar, Graphics malla)
		{
			try {
				if(celdaPintar.estaExpandido)
					malla.DrawImage(celdaPintar.textura.imgTextura, celdaPintar.posicion);
			} catch (Exception e) {
				Debug.WriteLine("Fuera de limite");
				Debug.WriteLine(e.Message);
			}
			
		}
		
		void VerPropiedadesToolStripMenuItemClick(object sender, EventArgs e)
		{
			Celda bus = posicionCelda(botonDerecho);
			var lVis = Enumerable.Range(0, visitados.Count).Where(x => visitados[x] == bus).ToList();
			
			string visPrint = "";
			for(int j = 0; j < lVis.Count;)
			{
				visPrint += (lVis[j]+1).ToString();
				j++;
				if(j!=lVis.Count)
				{
					visPrint += ", ";
				}
			}
			
			
			if(bus == posicionInicial)
			{
				MessageBox.Show("\nTerreno: " + bus.textura.nombreTextura +
				                "\nID Terreno: " + bus.textura.idTextura +
				                "\nEs la posicion inicial" +
				                "\nVisitas: " + visPrint
				                , "Celda: (" + bus.columnaLetra + ", " + bus.fila + ")");
			}
			else if(bus == posicionFinal)
			{
				MessageBox.Show("\nTerreno: " + bus.textura.nombreTextura +
				                "\nID Terreno: " + bus.textura.idTextura +
				                "\nEs la posicion final" +
				                "\nVisitas: " + visPrint
				                , "Celda: (" + bus.columnaLetra + ", " + bus.fila + ")");
			}
			else if(bus == posicionInicial && bus.posicion == posicionActualPersonaje)
			{
				MessageBox.Show("\nTerreno: " + bus.textura.nombreTextura +
				                "\nID Terreno: " + bus.textura.idTextura +
				                "\nEs la posicion inicial" +
				                "\nEn esta celda se encuentra el personaje" +
				                "\nVisitas: " + visPrint
				                , "Celda: (" + bus.columnaLetra + ", " + bus.fila + ")");
			}
			else if(bus == posicionFinal && bus.posicion == posicionActualPersonaje)
			{
				MessageBox.Show("\nTerreno: " + bus.textura.nombreTextura +
				                "\nID Terreno: " + bus.textura.idTextura +
				                "\nEs la posicion final" +
				                "\nEn esta celda se encuentra el personaje" +
				                "\nVisitas: " + visPrint
				                , "Celda: (" + bus.columnaLetra + ", " + bus.fila + ")");
			}
			else
			{
				MessageBox.Show("\nTerreno: " + bus.textura.nombreTextura +
				                "\nID Terreno: " + bus.textura.idTextura +
				                "\nVisitas: " + visPrint
				                , "Celda: (" + bus.columnaLetra + ", " + bus.fila + ")");
			}
		}
		
		float buscarPeso(string _idTextura)
		{
			for(int i = 0; i < personajeActivo.pesos.Count; i++)
			{
				if(personajeActivo.pesos[i].idTextura == _idTextura)
					return personajeActivo.pesos[i].peso;
			}
			return -2;
		}
		
		void LaberintoKeyDown(object sender, KeyEventArgs e)
		{
			try
			{
				if (banInicialColocada && banFinalColocada) {
					Graphics personaje = pictureBox1.CreateGraphics();
					RectangleF ultimaPos = posicionActualPersonaje;
					Pen lapiz = new Pen(Color.White, 1);
					
					Debug.WriteLine(e.KeyData);
					RectangleF nuevaPosArriba = new RectangleF(ultimaPos.X, ultimaPos.Y - altoCelda, largoCelda, altoCelda);
					RectangleF nuevaPosDerecha = new RectangleF(ultimaPos.X + largoCelda, ultimaPos.Y, largoCelda, altoCelda);
					RectangleF nuevaPosAbajo = new RectangleF(ultimaPos.X, ultimaPos.Y + altoCelda, largoCelda, altoCelda);
					RectangleF nuevaPosIzquierda = new RectangleF(ultimaPos.X - largoCelda, ultimaPos.Y, largoCelda, altoCelda);
					
					
					
					
					switch (e.KeyCode) {
						case Keys.Up:
							{
								
								Celda valCelda = posicionCelda(new PointF(nuevaPosArriba.X + 1, nuevaPosArriba.Y + 1));
								if(buscarPeso(valCelda.textura.idTextura) >= 0)
								{
									if (nuevaPosArriba.Y >= altoCelda-5) {
										//Limpia la textura anterior
										cargarTextura(posicionCelda(new PointF(ultimaPos.X + 1, ultimaPos.Y + 1)), personaje);
										//Recarga bandera Inicial
										if (compararFloats(posicionInicial.posicion.X, ultimaPos.X, 0.000001f) && compararFloats(posicionInicial.posicion.Y, ultimaPos.Y, 0.000001f)) {
											Image banInicio = Resource1.bandera_inicio;
											personaje.DrawImage(banInicio, ultimaPos);
										}
										//Recarga bandera Final
										if (compararFloats(posicionFinal.posicion.X, ultimaPos.X, 0.000001f) && compararFloats(posicionFinal.posicion.Y, ultimaPos.Y, 0.000001f)) {
											Image banFinal = Resource1.bandera_meta;
											personaje.DrawImage(banFinal, ultimaPos);
										}
										
										posicionCelda(new PointF(nuevaPosArriba.X+5, nuevaPosArriba.Y+5)).estaExpandido = true;
										cargarTextura(posicionCelda(new PointF(nuevaPosArriba.X+5, nuevaPosArriba.Y+5)),personaje);
										dibujarCeldasVecinas(new PointF(nuevaPosArriba.X+5, nuevaPosArriba.Y+5));
										
										cargarPersonaje(personaje, nuevaPosArriba);
										posicionActualPersonaje = nuevaPosArriba;
										visitados.Add(posicionCelda(new PointF(nuevaPosArriba.X+5, nuevaPosArriba.Y+5)));
										numVisita++;
										dibujarVisitas(personaje);
										sLPosicion.Text = "Columna: " + valCelda.columnaLetra + " Fila: " + valCelda.fila;
										sLTextura.Text = "Terreno: " + valCelda.textura.nombreTextura;
										mensajeFinal(posicionActualPersonaje);
									}
								}
							}
							break;
						case Keys.Right:
							{
								Celda valCelda = posicionCelda(new PointF(nuevaPosDerecha.X + 1, nuevaPosDerecha.Y + 1));
								if(buscarPeso(valCelda.textura.idTextura) >= 0)
								{
									if (nuevaPosDerecha.X <= pictureBox1.Size.Width - largoCelda) {
										cargarTextura(posicionCelda(new PointF(ultimaPos.X + 1, ultimaPos.Y + 1)), personaje);
										if (compararFloats(posicionInicial.posicion.X, ultimaPos.X, 0.000001f) && compararFloats(posicionInicial.posicion.Y, ultimaPos.Y, 0.000001f)) {
											Image banInicio = Resource1.bandera_inicio;
											personaje.DrawImage(banInicio, ultimaPos);
										}
										if (compararFloats(posicionFinal.posicion.X, ultimaPos.X, 0.000001f) && compararFloats(posicionFinal.posicion.Y, ultimaPos.Y, 0.000001f)) {
											Image banFinal = Resource1.bandera_meta;
											personaje.DrawImage(banFinal, ultimaPos);
										}
										
										posicionCelda(new PointF(nuevaPosDerecha.X+5, nuevaPosDerecha.Y+5)).estaExpandido = true;
										cargarTextura(posicionCelda(new PointF(nuevaPosDerecha.X+5, nuevaPosDerecha.Y+5)),personaje);
										dibujarCeldasVecinas(new PointF(nuevaPosDerecha.X+5, nuevaPosDerecha.Y+5));
										
										cargarPersonaje( personaje, nuevaPosDerecha);
										posicionActualPersonaje = nuevaPosDerecha;
										visitados.Add(posicionCelda(new PointF(nuevaPosDerecha.X+5, nuevaPosDerecha.Y+5)));
										numVisita++;
										dibujarVisitas(personaje);
										sLPosicion.Text = "Columna: " + valCelda.columnaLetra + " Fila: " + valCelda.fila;
										sLTextura.Text = "Terreno: " + valCelda.textura.nombreTextura;
										mensajeFinal(posicionActualPersonaje);
									}
								}
							}
							break;
						case Keys.Down:
							{
								Celda valCelda = posicionCelda(new PointF(nuevaPosAbajo.X + 1, nuevaPosAbajo.Y + 1));
								if(buscarPeso(valCelda.textura.idTextura) >= 0)
								{
									if (nuevaPosAbajo.Y <= pictureBox1.Size.Height - altoCelda) {
										cargarTextura(posicionCelda(new PointF(ultimaPos.X + 1, ultimaPos.Y + 1)), personaje);
										if (compararFloats(posicionInicial.posicion.X, ultimaPos.X, 0.000001f) && compararFloats(posicionInicial.posicion.Y, ultimaPos.Y, 0.000001f)) {
											Image banInicio = Resource1.bandera_inicio;
											personaje.DrawImage(banInicio, ultimaPos);
										}
										if (compararFloats(posicionFinal.posicion.X, ultimaPos.X, 0.000001f) && compararFloats(posicionFinal.posicion.Y, ultimaPos.Y, 0.000001f)) {
											Image banFinal = Resource1.bandera_meta;
											personaje.DrawImage(banFinal, ultimaPos);
										}
										
										posicionCelda(new PointF(nuevaPosAbajo.X+5, nuevaPosAbajo.Y+5)).estaExpandido = true;
										cargarTextura(posicionCelda(new PointF(nuevaPosAbajo.X+5, nuevaPosAbajo.Y+5)),personaje);
										dibujarCeldasVecinas(new PointF(nuevaPosAbajo.X+5, nuevaPosAbajo.Y+5));
										
										cargarPersonaje(personaje, nuevaPosAbajo);
										posicionActualPersonaje = nuevaPosAbajo;
										visitados.Add(posicionCelda(new PointF(nuevaPosAbajo.X+5, nuevaPosAbajo.Y+5)));
										numVisita++;
										dibujarVisitas(personaje);
										sLPosicion.Text = "Columna: " + valCelda.columnaLetra + " Fila: " + valCelda.fila;
										sLTextura.Text = "Terreno: " + valCelda.textura.nombreTextura;
										mensajeFinal(posicionActualPersonaje);
									}
								}
							}
							break;
						case Keys.Left:
							{
								Celda valCelda = posicionCelda(new PointF(nuevaPosIzquierda.X + 1, nuevaPosIzquierda.Y + 1));
								if(buscarPeso(valCelda.textura.idTextura) >= 0)
								{
									if (nuevaPosIzquierda.X >= largoCelda) {
										cargarTextura(posicionCelda(new PointF(ultimaPos.X + 1, ultimaPos.Y + 1)), personaje);
										if (compararFloats(posicionInicial.posicion.X, ultimaPos.X, 0.000001f) && compararFloats(posicionInicial.posicion.Y, ultimaPos.Y, 0.000001f)) {
											Image banInicio = Resource1.bandera_inicio;
											personaje.DrawImage(banInicio, ultimaPos);
										}
										if (compararFloats(posicionFinal.posicion.X, ultimaPos.X, 0.000001f) && compararFloats(posicionFinal.posicion.Y, ultimaPos.Y, 0.000001f)) {
											Image banFinal = Resource1.bandera_meta;
											personaje.DrawImage(banFinal, ultimaPos);
										}
										
										posicionCelda(new PointF(nuevaPosIzquierda.X+5, nuevaPosIzquierda.Y+5)).estaExpandido = true;
										cargarTextura(posicionCelda(new PointF(nuevaPosIzquierda.X+5, nuevaPosIzquierda.Y+5)),personaje);
										dibujarCeldasVecinas(new PointF(nuevaPosIzquierda.X+5, nuevaPosIzquierda.Y+5));
										
										cargarPersonaje(personaje, nuevaPosIzquierda);
										posicionActualPersonaje = nuevaPosIzquierda;
										visitados.Add(posicionCelda(new PointF(nuevaPosIzquierda.X+5, nuevaPosIzquierda.Y+5)));
										numVisita++;
										dibujarVisitas(personaje);
										sLPosicion.Text = "Columna: " + valCelda.columnaLetra + " Fila: " + valCelda.fila;
										sLTextura.Text = "Terreno: " + valCelda.textura.nombreTextura;
										mensajeFinal(posicionActualPersonaje);
									}
								}
							}
							break;
					}
				}
			}
			catch(Exception ex)
			{
				Debug.WriteLine("Pos no valida\n" + ex);
			}
			
			
		}
		
		bool compararFloats(float a, float b, float epsilon) {
			float absA = Math.Abs(a);
			float absB = Math.Abs(b);
			float diff = Math.Abs(a - b);

			if (a == b)
			{
				return true;
			}
			else if (a == 0 || b == 0 || diff < float.MinValue) {
				return diff < (epsilon * float.MinValue);
			}
			else
			{
				return diff / (absA + absB) < epsilon;
			}
		}
		
		/* Repinta las banderas inicial y final
		 * en caso de ser necesario
		 */
		
		void recargarBanderas(Graphics malla)
		{
			if (banInicialColocada || banFinalColocada) {
				Image banInicio = Resource1.bandera_inicio;
				malla.DrawImage(banInicio, posicionInicial.posicion);
				Image banFinal = Resource1.bandera_meta;
				malla.DrawImage(banFinal, posicionFinal.posicion);
			}
			
		}
		
		void cargarPersonaje(Graphics malla, RectangleF posicion)
		{
			if (personajeColocado && crearPersonajes.existePersonaje) {
				malla.DrawImage(personajeActivo.img, posicion);
			}
			
		}
		
		void limpiarVisitas(Graphics malla)
		{
			for(int i = 0; i < visitados.Count; i++)
			{
				cargarTextura(visitados[i], malla);
			}
			visitados.Clear();
		}
		
		void dibujarVisitas(Graphics malla)
		{
			for (int i = 0; i < numVisita; i++) {
				int j1 = 0;
				var lVis = Enumerable.Range(0, visitados.Count).Where(x => visitados[x] == visitados[i]).ToList();
				
				for(int j = 0; j < lVis.Count;)
				{
					if(j<2)
					{
						malla.DrawString((lVis[j]+1).ToString(), new Font(FontFamily.GenericSansSerif, 9, FontStyle.Bold, GraphicsUnit.Pixel),
						                 new SolidBrush(Color.White), visitados[i].posicion.X + 1 + j*15, visitados[i].posicion.Y + 1);
						j++;
						if(j!=lVis.Count)
						{
							malla.DrawString(",", new Font(FontFamily.GenericSansSerif, 10, FontStyle.Bold, GraphicsUnit.Pixel),
							                 new SolidBrush(Color.White), visitados[i].posicion.X + 1 + j*10, visitados[i].posicion.Y + 1);
						}
					}
					else
					{
						malla.DrawString((lVis[j]+1).ToString(), new Font(FontFamily.GenericSansSerif, 9, FontStyle.Bold, GraphicsUnit.Pixel),
						                 new SolidBrush(Color.White), visitados[i].posicion.X + 1 + j1*15, visitados[i].posicion.Y + 13);
						j++;
						if(j!=lVis.Count)
						{
							malla.DrawString(",", new Font(FontFamily.GenericSansSerif, 10, FontStyle.Bold, GraphicsUnit.Pixel),
							                 new SolidBrush(Color.White), visitados[i].posicion.X + 1 + j1*10, visitados[i].posicion.Y + 13);
						}
						j1++;
					}
				}
			}
		}
		
		void CargarLaberintoToolStripMenuItemClick(object sender, EventArgs e)
		{
			DialogResult cerrar = MessageBox.Show("Si cambia de laberinto perdera los personajes almacenados, ¿Seguro que desea realizar esta acción?", "Advertencia", MessageBoxButtons.YesNo);
			if (cerrar == DialogResult.Yes) {
				
				OpenFileDialog vBuscarArchivo = new OpenFileDialog();
				vBuscarArchivo.Filter = "Archivos de Texto|*.txt";
				vBuscarArchivo.Title = "Seleccione el Mapa";
				
				if (vBuscarArchivo.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
					try {
						using (System.IO.StreamReader lectorArchivo = new System.IO.StreamReader(vBuscarArchivo.FileName)) {
							String nuevoLaberinto = lectorArchivo.ReadToEnd();
							lectorArchivo.Close();
							MessageBox.Show(nuevoLaberinto, "String"); //Fines de pruebas
							
							Parseo labParseado = new Parseo(nuevoLaberinto);
							
							
							if (labParseado.esLaberintoValido()) {
								MessageBox.Show("Caracteres Validos");
								labParseado.formarMatriz();
								FormTextura nuevaTextura = new FormTextura(labParseado.matrizDeLaberinto, labParseado.cantidadFilas, labParseado.cantidadColumnas);
								
								//Descomentar si se desea visualizar el contenido de la matriz
								//labParseado.imprimirMatriz();
								nuevaTextura.Show();
								this.Hide();
								
							}
						}
					} catch (Exception ex) {
						MessageBox.Show("El archivo no pudo ser leido", "Error de lectura");
						MessageBox.Show(ex.Message, "Error");
						Debug.WriteLine("El archivo no pudo ser leido");
						Debug.WriteLine(ex.Message);
					}
				} else {
					MessageBox.Show("No has seleccionado ningun archivo", "Advertencia");
					Console.WriteLine("El archivo no pudo ser leido");
				}
				
			} else if (cerrar == DialogResult.No) {
				
			}
			
			
		}
		void LaberintoFormClosing(object sender, FormClosingEventArgs e)
		{
			DialogResult cerrar = MessageBox.Show("¿Realmente quiere salir del programa?", "Cerrar", MessageBoxButtons.YesNo);
			if (cerrar == DialogResult.Yes) {
				Application.ExitThread();
			} else if (cerrar == DialogResult.No) {
				e.Cancel = true;
			}
		}
		void CrearPersonajesToolStripMenuItemClick(object sender, EventArgs e)
		{
			if(listaPersonajes.Count<=4)
			{
				crearPersonajes.Show();
				Debug.WriteLine("Creando Personaje");
			}
			else
			{
				MessageBox.Show("Ya no se pueden crear más personajes");
			}
			
			
		}
		
		/* Si la posicion evaluada es igual a la final
		 * se gana el juego y se regresa un true
		 */
		
		bool mensajeFinal(RectangleF posActual)
		{
			
			if(compararFloats(posActual.X, posicionFinal.posicion.X, 0.000001f) && compararFloats(posActual.Y, posicionFinal.posicion.Y, 0.000001f))
			{
				MessageBox.Show("¡Felicidades, eres un ganador!\nHas encontrado la meta en " +
				                posicionCelda(new PointF(posicionActualPersonaje.X, posicionActualPersonaje.Y)).columnaLetra +
				                ", " + posicionCelda(new PointF(posicionActualPersonaje.X, posicionActualPersonaje.Y)).fila ,"WINNER");
				
				return true;
			}
			return false;
		}
		void ToolStripTextBox1TextChanged(object sender, EventArgs e)
		{
			try {
				
				string x = tSBuscar.Text.Substring(0,1);
				string y = tSBuscar.Text.Substring(2,1);
				
				int _X = 0;
				
				foreach (char c in x.ToString()) {
					_X = _X*26 + c - 'A' + 1;
				}
				
				int _Y = Int32.Parse(y.ToString());
				
				Celda temp;
				
				for (int i = 0; i < nFilas; i++) {
					for (int j = 0; j < nColumnas; j++) {
						if (matrizCeldas[i, j].posicion.Contains(new PointF(_X, _Y))) {
							temp = matrizCeldas[i,j];
							MessageBox.Show("Textura: " + temp.textura.nombreTextura);
						}
					}
					
				}
				
			} catch (Exception ex) {
				Debug.WriteLine("No existe la celda");
				Debug.WriteLine(ex);
			}
		}
		void TSBuscarKeyDown(object sender, KeyEventArgs e)
		{
			try
			{
				Debug.WriteLine("Buscar");
				switch(e.KeyCode)
				{
					case Keys.Enter:
						{
							const String regex = @"[A-Z],[0-9][0-9]?";
							MatchCollection match = Regex.Matches(tSBuscar.Text, regex);
							tSBuscar.Text = "";
							if(match.Count == 0)
							{
								MessageBox.Show("Tu busqueda no es correcta \nIntenta colocando la letra de la columna primero en mayuscula,"+
								                " seguido de una coma y finalizando con el número de la fila"
								                + "\nTodo sin espacios en blanco");
							}
							else
							{
								string[] a = match[0].Value.Split(',');
								char x = a[0][0];
								
								string y = a[1];
								
								int _X = x-64;
								int _Y = Int32.Parse(y);
								
								Celda bus = posicionCelda(new PointF(_X*largoCelda+1, _Y*altoCelda+1));
								
								var lVis = Enumerable.Range(0, visitados.Count).Where(t => visitados[t] == bus).ToList();
								
								string visPrint = "";
								for(int j = 0; j < lVis.Count;)
								{
									visPrint += (lVis[j]+1).ToString();
									j++;
									if(j!=lVis.Count)
									{
										visPrint += ", ";
									}
								}
								
								if(bus == posicionInicial)
								{
									MessageBox.Show("\nTerreno: " + bus.textura.nombreTextura +
									                "\nID Terreno: " + bus.textura.idTextura +
									                "\nEs la posicion inicial" +
									                "\nVisitas: " + visPrint
									                , "Celda: (" + bus.columnaLetra + ", " + bus.fila + ")");
								}
								else if(bus == posicionFinal)
								{
									MessageBox.Show("\nTerreno: " + bus.textura.nombreTextura +
									                "\nID Terreno: " + bus.textura.idTextura +
									                "\nEs la posicion final" +
									                "\nVisitas: " + visPrint
									                , "Celda: (" + bus.columnaLetra + ", " + bus.fila + ")");
								}
								else if(bus == posicionInicial && bus.posicion.Contains(posicionActualPersonaje))
								{
									MessageBox.Show("\nTerreno: " + bus.textura.nombreTextura +
									                "\nID Terreno: " + bus.textura.idTextura +
									                "\nEs la posicion inicial" +
									                "\nEn esta celda se encuentra el personaje" +
									                "\nVisitas: " + visPrint
									                , "Celda: (" + bus.columnaLetra + ", " + bus.fila + ")");
								}
								else if(bus == posicionFinal && bus.posicion.Contains(posicionActualPersonaje))
								{
									MessageBox.Show("\nTerreno: " + bus.textura.nombreTextura +
									                "\nID Terreno: " + bus.textura.idTextura +
									                "\nEs la posicion final" +
									                "\nEn esta celda se encuentra el personaje" +
									                "\nVisitas: " + visPrint
									                , "Celda: (" + bus.columnaLetra + ", " + bus.fila + ")");
								}
								else
								{
									MessageBox.Show("\nTerreno: " + bus.textura.nombreTextura +
									                "\nID Terreno: " + bus.textura.idTextura +
									                "\nVisitas: " + visPrint
									                , "Celda: (" + bus.columnaLetra + ", " + bus.fila + ")");
								}
								
							}
						}
						break;
					default:
						return;
				}
			}
			catch(Exception ex)
			{
				Debug.WriteLine(ex);
			}
		}
		
		private Celda posicionCelda(RectangleF celda)
		{
			try {
				for (int i = 0; i < nFilas; i++) {
					for (int j = 0; j < nColumnas; j++) {
						if (matrizCeldas[i, j].posicion.Contains(celda)) {
							return matrizCeldas[i, j];
						}
					}
					
				}
				return null;
				
			} catch (Exception e) {
				Debug.WriteLine("No existe la celda");
				Debug.WriteLine(e);
				return null;
			}
		}
		
		void Handler_PersonajeSeleccion(object sender, EventArgs e)
		{
			
			ToolStripMenuItem personajeSelec = sender as ToolStripMenuItem;
			personajeActivo = listaPersonajes.Find(x => x.nombrePersonaje == personajeSelec.Text);
		}
		
		void PersonajesToolStripMenuItemClick(object sender, EventArgs e)
		{
			listaPersonajes = crearPersonajes.objPersonaje;
			if(listaPersonajes.Count != 0)
			{
				seleccionarPersonajesToolStripMenuItem.Visible = true;
				seleccionarPersonajesToolStripMenuItem.Enabled = true;
				seleccionarPersonajesToolStripMenuItem.DropDownItems.Clear();
				for(int i = 0; i < listaPersonajes.Count; i++)
				{
					ToolStripMenuItem personaje = new ToolStripMenuItem();
					
					personaje.Name = "personaje" + i.ToString();
					personaje.Size = new Size(120,22);
					personaje.Text = listaPersonajes[i].nombrePersonaje;
					personaje.Image = listaPersonajes[i].img;
					
					personaje.Click += new EventHandler(Handler_PersonajeSeleccion);
					
					seleccionarPersonajesToolStripMenuItem.DropDownItems.Add(personaje);
				}
			}
		}
		void SobreToolStripMenuItemClick(object sender, EventArgs e)
		{
			Form a = new Form();
			a.StartPosition = FormStartPosition.CenterScreen;
			a.Text = "Laberinto Inteligente";
			
			
			Label tb = new Label();
			tb.Size = new Size(a.Size.Width, a.Size.Height);
			tb.TextAlign = ContentAlignment.MiddleCenter;
			tb.Text = "Este software fue creado por:\nCésar Arley Ojeda Escobar\nMontserrat Delgado Alvarez";
			tb.Font = new Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			tb.ForeColor = Color.Tomato;
			
			tb.BackColor = Color.DimGray;
			
			a.Controls.Add(tb);
			
			a.Show();
		}
	}
}