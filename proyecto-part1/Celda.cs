﻿/*
 * Created by SharpDevelop.
 * User: CesarArleyOE
 * Date: 19/03/2019
 * Time: 12:45 a. m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Diagnostics;

namespace proyecto_part1
{
	/// <summary>
	/// Description of Celda.
	/// </summary>
	public class Celda
	{
		public int fila;
		public int columna;
		public char columnaLetra;
		public RectangleF posicion;
		public Textura textura;
		public bool estaExpandido;
		
		public Celda()
		{
			
		}
		
		public Celda(int _fila, int _columna, RectangleF _posicion, Textura _textura)
		{
			this.fila = _fila;
			this.columna = _columna;
			this.posicion = _posicion;
			this.textura = _textura;
			convertirColumnaIndice();
		}
		
		void convertirColumnaIndice()
		{
			columnaLetra = (char)(columna+64);
		}
	}
}
